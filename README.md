# Securing-NFS

Demonstration on how to secure NFS for Openshift 4.6

## References
[Introduction to NFS](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/ch-nfs)

## Hardening
[NFS Security](https://web.mit.edu/rhel-doc/5/RHEL-5-manual/Deployment_Guide-en-US/s1-nfs-security.html)

- /etc/exports example config (white listing instead of *)

```
/export/registry *(rw,root_squash)
/export/ocp-pv *(rw,root_squash)
```
`exportfs -rva`

- create user to limit access with UID in [OCP UIDs](https://www.openshift.com/blog/a-guide-to-openshift-and-uids)

`oc describe project <project>`

```
sudo adduser -M ocpnfs
sudo usermod -u 1000920000 ocpnfs
sudo groupmod -g 1000920000 ocpnfs
sudo chown -R ocpnfs:ocpnfs /export
id ocpnfs
```
- set permissions 

```
sudo chmod -R 760 /export
sudo setenforce 1
sudo setsebool -P virt_use_nfs 1
```

- open firewall nfs ports
```
sudo iptables -I INPUT 1 -p tcp --dport 2049 -j ACCEPT
sudo iptables -I INPUT 1 -p tcp --dport 20048 -j ACCEPT
sudo iptables -I INPUT 1 -p tcp --dport 111 -j ACCEPT
```


## Openshift and NFS
[OCP -Docs](https://docs.openshift.com/container-platform/4.6/storage/persistent_storage/persistent-storage-nfs.html)

- create a project and resources 
-- updated ocp template: removed pvc creation, namespace updated dc claim name, security context 

```
oc new-project nfs-db-example

oc create -n nfs-db-example -f- <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-db-pv1111
spec:
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  nfs:
    server: 10.10.10.5
    path: "/export/ocp-pv"
EOF


oc create -n nfs-db-example -f- <<EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: db-claim-maria 
spec:
  accessModes:
  - ReadWriteMany
  storageClassName: ""
  resources:
    requests:
      storage: "1Gi"
  volumeName: nfs-db-pv1111
EOF


_There has been an issue with the following command and it may be easier to put inside a file and do the oc creae -f with that_

oc create  -f-  <<EOF
apiVersion: template.openshift.io/v1
kind: Template
labels:
  template: mariadb-persistent-template
message: |-
  The following service(s) have been created in your project: ${DATABASE_SERVICE_NAME}.
         Username: ${MYSQL_USER}
         Password: ${MYSQL_PASSWORD}
    Database Name: ${MYSQL_DATABASE}
   Connection URL: mysql://${DATABASE_SERVICE_NAME}:3306/
  For more information about using this template, including OpenShift considerations, see https://github.com/sclorg/mariadb-container/blob/master/10.3/root/usr/share/container-scripts/mysql/README.md.
metadata:
  annotations:
    description: |-
      MariaDB database service, with persistent storage. For more information about using this template, including OpenShift considerations, see https://github.com/sclorg/mariadb-container/blob/master/10.3/root/usr/share/container-scripts/mysql/README.md.
      NOTE: Scaling to more than one replica is not supported. You must have persistent volumes available in your cluster to use this template.
    iconClass: icon-mariadb
    openshift.io/display-name: MariaDB
    openshift.io/documentation-url: https://github.com/sclorg/mariadb-container/blob/master/10.3/root/usr/share/container-scripts/mysql/README.md
    openshift.io/long-description: This template provides a standalone MariaDB server with a database created.  The database is stored on persistent storage.  The database name, username, and password are chosen via parameters when provisioning this service.
    openshift.io/provider-display-name: Red Hat, Inc.
    openshift.io/support-url: https://access.redhat.com
    samples.operator.openshift.io/version: 4.6.9
    tags: database,mariadb
  creationTimestamp: "2021-01-04T18:11:22Z"
  labels:
    samples.operator.openshift.io/managed: "true"
  managedFields:
  - apiVersion: template.openshift.io/v1
    fieldsType: FieldsV1
    fieldsV1:
      f:labels:
        .: {}
        f:template: {}
      f:message: {}
      f:metadata:
        f:annotations:
          .: {}
          f:description: {}
          f:iconClass: {}
          f:openshift.io/display-name: {}
          f:openshift.io/documentation-url: {}
          f:openshift.io/long-description: {}
          f:openshift.io/provider-display-name: {}
          f:openshift.io/support-url: {}
          f:samples.operator.openshift.io/version: {}
          f:tags: {}
        f:labels:
          .: {}
          f:samples.operator.openshift.io/managed: {}
      f:objects: {}
      f:parameters: {}
    manager: cluster-samples-operator
    operation: Update
    time: "2021-01-04T18:11:22Z"
  name: mymaria
  namespace: nfs-db-example
objects:
- apiVersion: v1
  kind: Secret
  metadata:
    annotations:
      template.openshift.io/expose-database_name: '{.data[''database-name'']}'
      template.openshift.io/expose-password: '{.data[''database-password'']}'
      template.openshift.io/expose-root_password: '{.data[''database-root-password'']}'
      template.openshift.io/expose-username: '{.data[''database-user'']}'
    name: ${DATABASE_SERVICE_NAME}
  stringData:
    database-name: ${MYSQL_DATABASE}
    database-password: ${MYSQL_PASSWORD}
    database-root-password: ${MYSQL_ROOT_PASSWORD}
    database-user: ${MYSQL_USER}
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      template.openshift.io/expose-uri: mysql://{.spec.clusterIP}:{.spec.ports[?(.name=="mariadb")].port}
    name: ${DATABASE_SERVICE_NAME}
  spec:
    ports:
    - name: mariadb
      port: 3306
    selector:
      name: ${DATABASE_SERVICE_NAME}
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    annotations:
      template.alpha.openshift.io/wait-for-ready: "true"
    name: ${DATABASE_SERVICE_NAME}
  spec:
    replicas: 1
    selector:
      name: ${DATABASE_SERVICE_NAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: ${DATABASE_SERVICE_NAME}
      spec:
        containers:
        - env:
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                key: database-user
                name: ${DATABASE_SERVICE_NAME}
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-password
                name: ${DATABASE_SERVICE_NAME}
          - name: MYSQL_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-root-password
                name: ${DATABASE_SERVICE_NAME}
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                key: database-name
                name: ${DATABASE_SERVICE_NAME}
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            exec:
              command:
              - /bin/sh
              - -i
              - -c
              - MYSQL_PWD="$MYSQL_PASSWORD" mysqladmin -u $MYSQL_USER ping
            initialDelaySeconds: 30
            timeoutSeconds: 1
          name: mariadb
          ports:
          - containerPort: 3306
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - -i
              - -c
              - MYSQL_PWD="$MYSQL_PASSWORD" mysqladmin -u $MYSQL_USER ping
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              memory: ${MEMORY_LIMIT}
          securityContext:
            runAsUser: 1000760000
          volumeMounts:
          - mountPath: /var/lib/mysql/data
            name: ${DATABASE_SERVICE_NAME}-data
        volumes:
        - name: ${DATABASE_SERVICE_NAME}-data
          persistentVolumeClaim:
            claimName: db-claim-maria
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - mariadb
        from:
          kind: ImageStreamTag
          name: mariadb:${MARIADB_VERSION}
          namespace: ${NAMESPACE}
      type: ImageChange
    - type: ConfigChange
parameters:
- description: Maximum amount of memory the container can use.
  displayName: Memory Limit
  name: MEMORY_LIMIT
  required: true
  value: 512Mi
- description: The OpenShift Namespace where the ImageStream resides.
  displayName: Namespace
  name: NAMESPACE
  value: openshift
- description: The name of the OpenShift Service exposed for the database.
  displayName: Database Service Name
  name: DATABASE_SERVICE_NAME
  required: true
  value: mariadb
- description: Username for MariaDB user that will be used for accessing the database.
  displayName: MariaDB Connection Username
  from: user[A-Z0-9]{3}
  generate: expression
  name: MYSQL_USER
  required: true
- description: Password for the MariaDB connection user.
  displayName: MariaDB Connection Password
  from: '[a-zA-Z0-9]{16}'
  generate: expression
  name: MYSQL_PASSWORD
  required: true
- description: Password for the MariaDB root user.
  displayName: MariaDB root Password
  from: '[a-zA-Z0-9]{16}'
  generate: expression
  name: MYSQL_ROOT_PASSWORD
  required: true
- description: Name of the MariaDB database accessed.
  displayName: MariaDB Database Name
  name: MYSQL_DATABASE
  required: true
  value: sampledb
- description: Version of MariaDB image to be used (10.3-el7, 10.3-el8, or latest).
  displayName: Version of MariaDB Image
  name: MARIADB_VERSION
  required: true
  value: 10.3-el8
- description: Volume space available for data, e.g. 512Mi, 2Gi.
  displayName: Volume Capacity
  name: VOLUME_CAPACITY
  required: true
  value: 1Gi
EOF

oc new-app --template=mymaria

oc rollout pause dc mariadb

```

- patch or update deployment to include UID to run as or supplemental groups


```
oc patch dc mariadb -n  nfs-db-example --type merge --patch '{"spec":{"template":{"spec":{"securityContext":{"supplementalGroups":[1000620001]}}}}}'

oc patch dc mariadb -n  nfs-db-example  --type merge --patch '{"spec":{"template":{"spec":{"containers":{"securityContext":{"runAsUser": "1000721000"}}}}}}'

oc rollout resume dc mariadb
```





## Openshift Example Image Registry
[OCP-Docs](https://docs.openshift.com/container-platform/4.6/registry/configuring_registry_storage/configuring-registry-storage-vsphere.html#registry-configuring-storage-vsphere_configuring-registry-storage-vsphere)

- create pv 

```
oc create -n openshift-image-registry -f- <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-registry0
spec:
  capacity:
    storage: 105Gi
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  nfs:
    server: 10.10.10.5
    path: "/export/registry"
EOF
```



- create pvc 

```
oc create -n openshift-image-registry -f- <<EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: registry-claim 
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: "100Gi"
EOF
```


- Set the image registry to managed mode

`oc patch configs.imageregistry.operator.openshift.io cluster --type merge --patch '{"spec":{"managementState":"Managed"}}'`


- update image registry to use nfs pvc claim

`oc patch configs.imageregistry.operator.openshift.io cluster --type merge --patch '{"spec":{"storage":{"pvc":{"claim":"registry-claim"}}}}'`

- update deployment to give pods supplemental group not at container level

`oc patch deploy image-registry -n openshift-image-registry --type merge --patch '{"spec":{"spec":{"securityContext":{"supplementalGroups":[<groupid>]}}}}'`

```
spec:
  containers:
    - name:
    ...
  securityContext: 
    supplementalGroups: [5555]
  ```

- update deployment to give containers run as security context

`oc patch deploy image-registry -n openshift-image-registry --type merge --patch '{"spec":{"spec":{"containers":{"securityContext":{"runAsUser": "<uid>"}}}}}'`

```
spec:
  containers: 
  - name:
  ...
    securityContext:
      runAsUser: <uid> 
```




nfs-operator [opensource project](https://two-oes.medium.com/working-with-nfs-as-a-storageclass-in-openshift-4-44367576771c)
